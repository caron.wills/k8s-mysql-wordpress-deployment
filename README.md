k8s-mysql-wordpress-deployment


Create cluster for applications:



1. Create credentials for AWS 

Download the AWS cli tools by running the following:

# pip install awscli --upgrade --user

followed by :

# aws configure 

Update /etc/profile  with the following environment variable  source . /etc/profile 

export AWS_ACCESS_KEY_ID=
export AWS_SECRET_ACCESS_KEY=

- Generate an RSA public key using ssh-keygen in /root/.ssh


- Deploy the k8s cluster with kops, instead of using terraform, it's faster cleaner and much less prone to error. 

Install Kops into /usr/local/bin

root@Helsing:/# cd /usr/local/bin
root@Helsing:/# curl -LO https://github.com/kubernetes/kops/releases/download/$(curl -s https://api.github.com/repos/kubernetes/kops/releases/latest | grep tag_name | cut -d '"' -f 4)/kops-linux-amd64


Install kubectl as you'll need this later to administer the cluster.

root@Helsing:/#  apt-get update
root@Helsing:/# apt-get install -y kubectl



2. Create an AWS S3 bucket for the kops configuration. 



root@Helsing:/usr/local/bin# aws s3api create-bucket --bucket sen-kops-state-store --region eu-west-1 --create-bucket-configuration LocationConstraint=eu-west-1

http://sen-kops-state-store.s3.amazonaws.com/


3. Provide the name of the cluster and set the bucket name in a environment variable, add these variable to /etc/profile

export KOPS_CLUSTER_NAME=sen.k8s.local
export KOPS_STATE_STORE=s3://sen-kops-state-store


put this into /etc/profile thus

export KOPS_CLUSTER_NAME=sen.k8s.local
export KOPS_STATE_STORE=s3://sen-kops-state-store


4. Create the cluster configuration using kops with the following syntax:

kops create cluster  --node-count=5  --node-size=t2.medium  --zones=eu-west-1a  --name=${KOPS_CLUSTER_NAME}


Results:
root@Helsing:/usr/local/bin# kops create cluster --node-count=5 --node-size=t2.medium --zones=eu-west-1a --name=sen.k8s.local
I1027 11:37:12.788448   28303 create_cluster.go:480] Inferred --cloud=aws from zone "eu-west-1a"
I1027 11:37:12.921855   28303 subnets.go:184] Assigned CIDR 172.20.32.0/19 to subnet eu-west-1a
I1027 11:37:13.657709   28303 create_cluster.go:1351] Using SSH public key: /root/.ssh/id_rsa.pub
Previewing changes that will be made:

I1027 11:37:15.177772   28303 apply_cluster.go:505] Gossip DNS: skipping DNS validation
I1027 11:37:15.197436   28303 executor.go:103] Tasks: 0 done / 77 total; 30 can run
I1027 11:37:15.672267   28303 executor.go:103] Tasks: 30 done / 77 total; 24 can run
I1027 11:37:15.883697   28303 executor.go:103] Tasks: 54 done / 77 total; 19 can run
I1027 11:37:16.052243   28303 executor.go:103] Tasks: 73 done / 77 total; 3 can run
W1027 11:37:16.079003   28303 keypair.go:140] Task did not have an address: *awstasks.LoadBalancer {"Name":"api.sen.k8s.local","Lifecycle":"Sync","LoadBalancerName":"api-sen-k8s-local-57ppce","DNSName":null,"HostedZoneId":null,"Subnets":[{"Name":"eu-west-1a.sen.k8s.local","ShortName":"eu-west-1a","Lifecycle":"Sync","ID":null,"VPC":{"Name":"sen.k8s.local","Lifecycle":"Sync","ID":null,"CIDR":"172.20.0.0/16","EnableDNSHostnames":true,"EnableDNSSupport":true,"Shared":false,"Tags":{"KubernetesCluster":"sen.k8s.local","Name":"sen.k8s.local","kubernetes.io/cluster/sen.k8s.local":"owned"}},"AvailabilityZone":"eu-west-1a","CIDR":"172.20.32.0/19","Shared":false,"Tags":{"KubernetesCluster":"sen.k8s.local","Name":"eu-west-1a.sen.k8s.local","SubnetType":"Public","kubernetes.io/cluster/sen.k8s.local":"owned","kubernetes.io/role/elb":"1"}}],"SecurityGroups":[{"Name":"api-elb.sen.k8s.local","Lifecycle":"Sync","ID":null,"Description":"Security group for api ELB","VPC":{"Name":"sen.k8s.local","Lifecycle":"Sync","ID":null,"CIDR":"172.20.0.0/16","EnableDNSHostnames":true,"EnableDNSSupport":true,"Shared":false,"Tags":{"KubernetesCluster":"sen.k8s.local","Name":"sen.k8s.local","kubernetes.io/cluster/sen.k8s.local":"owned"}},"RemoveExtraRules":["port=443"],"Shared":null,"Tags":{"KubernetesCluster":"sen.k8s.local","Name":"api-elb.sen.k8s.local","kubernetes.io/cluster/sen.k8s.local":"owned"}}],"Listeners":{"443":{"InstancePort":443,"SSLCertificateID":""}},"Scheme":null,"HealthCheck":{"Target":"SSL:443","HealthyThreshold":2,"UnhealthyThreshold":2,"Interval":10,"Timeout":5},"AccessLog":null,"ConnectionDraining":null,"ConnectionSettings":{"IdleTimeout":300},"CrossZoneLoadBalancing":null,"SSLCertificateID":""}
I1027 11:37:16.176956   28303 executor.go:103] Tasks: 76 done / 77 total; 1 can run
I1027 11:37:16.213295   28303 executor.go:103] Tasks: 77 done / 77 total; 0 can run
Will create resources:
  AutoscalingGroup/master-eu-west-1a.masters.sen.k8s.local
  	MinSize             	1
  	MaxSize             	1
  	Subnets             	[name:eu-west-1a.sen.k8s.local]
  	Tags                	{KubernetesCluster: sen.k8s.local, k8s.io/cluster-autoscaler/node-template/label/kops.k8s.io/instancegroup: master-eu-west-1a, k8s.io/role/master: 1, Name: master-eu-west-1a.masters.sen.k8s.local}
  	Granularity         	1Minute
  	Metrics             	[GroupDesiredCapacity, GroupInServiceInstances, GroupMaxSize, GroupMinSize, GroupPendingInstances, GroupStandbyInstances, GroupTerminatingInstances, GroupTotalInstances]
  	LaunchConfiguration 	name:master-eu-west-1a.masters.sen.k8s.local

  AutoscalingGroup/nodes.sen.k8s.local
  	MinSize             	5
  	MaxSize             	5
  	Subnets             	[name:eu-west-1a.sen.k8s.local]
  	Tags                	{KubernetesCluster: sen.k8s.local, k8s.io/cluster-autoscaler/node-template/label/kops.k8s.io/instancegroup: nodes, k8s.io/role/node: 1, Name: nodes.sen.k8s.local}
  	Granularity         	1Minute
  	Metrics             	[GroupDesiredCapacity, GroupInServiceInstances, GroupMaxSize, GroupMinSize, GroupPendingInstances, GroupStandbyInstances, GroupTerminatingInstances, GroupTotalInstances]
	LaunchConfiguration 	name:nodes.sen.k8s.local

  DHCPOptions/sen.k8s.local
  	DomainName          	eu-west-1.compute.internal
  	DomainNameServers   	AmazonProvidedDNS
  	Shared              	false
  	Tags                	{Name: sen.k8s.local, KubernetesCluster: sen.k8s.local, kubernetes.io/cluster/sen.k8s.local: owned}

  EBSVolume/a.etcd-events.sen.k8s.local
  	AvailabilityZone    	eu-west-1a
  	VolumeType          	gp2
  	SizeGB              	20
  	Encrypted           	false
  	Tags                	{k8s.io/role/master: 1, kubernetes.io/cluster/sen.k8s.local: owned, Name: a.etcd-events.sen.k8s.local, KubernetesCluster: sen.k8s.local, k8s.io/etcd/events: a/a}

  EBSVolume/a.etcd-main.sen.k8s.local
  	AvailabilityZone    	eu-west-1a
  	VolumeType          	gp2
  	SizeGB              	20
  	Encrypted           	false
  	Tags                	{k8s.io/etcd/main: a/a, k8s.io/role/master: 1, kubernetes.io/cluster/sen.k8s.local: owned, Name: a.etcd-main.sen.k8s.local, KubernetesCluster: sen.k8s.local}

  IAMInstanceProfile/masters.sen.k8s.local
  	Shared              	false

  IAMInstanceProfile/nodes.sen.k8s.local
  	Shared              	false

  IAMInstanceProfileRole/masters.sen.k8s.local
  	InstanceProfile     	name:masters.sen.k8s.local id:masters.sen.k8s.local
  	Role                	name:masters.sen.k8s.local

  IAMInstanceProfileRole/nodes.sen.k8s.local
  	InstanceProfile     	name:nodes.sen.k8s.local id:nodes.sen.k8s.local
  	Role                	name:nodes.sen.k8s.local

  IAMRole/masters.sen.k8s.local
  	ExportWithID        	masters

  IAMRole/nodes.sen.k8s.local
  	ExportWithID        	nodes

  IAMRolePolicy/masters.sen.k8s.local
  	Role                	name:masters.sen.k8s.local

  IAMRolePolicy/nodes.sen.k8s.local
  	Role                	name:nodes.sen.k8s.local

  InternetGateway/sen.k8s.local
  	VPC                 	name:sen.k8s.local
  	Shared              	false
  	Tags                	{Name: sen.k8s.local, KubernetesCluster: sen.k8s.local, kubernetes.io/cluster/sen.k8s.local: owned}

  Keypair/apiserver-aggregator
  	Signer              	name:apiserver-aggregator-ca id:cn=apiserver-aggregator-ca
  	Subject             	cn=aggregator
  	Type                	client
  	Format              	v1alpha2

  Keypair/apiserver-aggregator-ca
  	Subject             	cn=apiserver-aggregator-ca
  	Type                	ca
  	Format              	v1alpha2

  Keypair/apiserver-proxy-client
  	Signer              	name:ca id:cn=kubernetes
  	Subject             	cn=apiserver-proxy-client
  	Type                	client
  	Format              	v1alpha2

  Keypair/ca
  	Subject             	cn=kubernetes
  	Type                	ca
  	Format              	v1alpha2

  Keypair/kops
  	Signer              	name:ca id:cn=kubernetes
  	Subject             	o=system:masters,cn=kops
  	Type                	client
  	Format              	v1alpha2

  Keypair/kube-controller-manager
  	Signer              	name:ca id:cn=kubernetes
  	Subject             	cn=system:kube-controller-manager
  	Type                	client
  	Format              	v1alpha2

  Keypair/kube-proxy
  	Signer              	name:ca id:cn=kubernetes
  	Subject             	cn=system:kube-proxy
  	Type                	client
  	Format              	v1alpha2

  Keypair/kube-scheduler
  	Signer              	name:ca id:cn=kubernetes
  	Subject             	cn=system:kube-scheduler
  	Type                	client
  	Format              	v1alpha2

  Keypair/kubecfg
  	Signer              	name:ca id:cn=kubernetes
  	Subject             	o=system:masters,cn=kubecfg
  	Type                	client
  	Format              	v1alpha2

  Keypair/kubelet
  	Signer              	name:ca id:cn=kubernetes
  	Subject             	o=system:nodes,cn=kubelet
  	Type                	client
  	Format              	v1alpha2

  Keypair/kubelet-api
  	Signer              	name:ca id:cn=kubernetes
  	Subject             	cn=kubelet-api
  	Type                	client
  	Format              	v1alpha2

  Keypair/master
  	AlternateNames      	[100.64.0.1, 127.0.0.1, api.internal.sen.k8s.local, api.sen.k8s.local, kubernetes, kubernetes.default, kubernetes.default.svc, kubernetes.default.svc.cluster.local]
  	Signer              	name:ca id:cn=kubernetes
  	Subject             	cn=kubernetes-master
  	Type                	server
  	Format              	v1alpha2

  LaunchConfiguration/master-eu-west-1a.masters.sen.k8s.local
  	ImageID             	kope.io/k8s-1.10-debian-jessie-amd64-hvm-ebs-2018-08-17
  	InstanceType        	m3.medium
  	SSHKey              	name:kubernetes.sen.k8s.local-ac:e0:1e:36:3b:cb:2a:ff:4d:ed:87:47:30:20:8a:b9 id:kubernetes.sen.k8s.local-ac:e0:1e:36:3b:cb:2a:ff:4d:ed:87:47:30:20:8a:b9
  	SecurityGroups      	[name:masters.sen.k8s.local]
  	AssociatePublicIP   	true
  	IAMInstanceProfile  	name:masters.sen.k8s.local id:masters.sen.k8s.local
  	RootVolumeSize      	64
  	RootVolumeType      	gp2
  	SpotPrice           	

  LaunchConfiguration/nodes.sen.k8s.local
  	ImageID             	kope.io/k8s-1.10-debian-jessie-amd64-hvm-ebs-2018-08-17
  	InstanceType        	t2.medium
  	SSHKey              	name:kubernetes.sen.k8s.local-ac:e0:1e:36:3b:cb:2a:ff:4d:ed:87:47:30:20:8a:b9 id:kubernetes.sen.k8s.local-ac:e0:1e:36:3b:cb:2a:ff:4d:ed:87:47:30:20:8a:b9
  	SecurityGroups      	[name:nodes.sen.k8s.local]
  	AssociatePublicIP   	true
  	IAMInstanceProfile  	name:nodes.sen.k8s.local id:nodes.sen.k8s.local
  	RootVolumeSize      	128
  	RootVolumeType      	gp2
  	SpotPrice           	

  LoadBalancer/api.sen.k8s.local
  	LoadBalancerName    	api-sen-k8s-local-57ppce
  	Subnets             	[name:eu-west-1a.sen.k8s.local]
  	SecurityGroups      	[name:api-elb.sen.k8s.local]
  	Listeners           	{443: {"InstancePort":443,"SSLCertificateID":""}}
  	HealthCheck         	{"Target":"SSL:443","HealthyThreshold":2,"UnhealthyThreshold":2,"Interval":10,"Timeout":5}
  	ConnectionSettings  	{"IdleTimeout":300}
  	SSLCertificateID    	

  LoadBalancerAttachment/api-master-eu-west-1a
  	LoadBalancer        	name:api.sen.k8s.local id:api.sen.k8s.local
  	AutoscalingGroup    	name:master-eu-west-1a.masters.sen.k8s.local id:master-eu-west-1a.masters.sen.k8s.local

  ManagedFile/sen.k8s.local-addons-bootstrap
  	Location            	addons/bootstrap-channel.yaml

  ManagedFile/sen.k8s.local-addons-core.addons.k8s.io
  	Location            	addons/core.addons.k8s.io/v1.4.0.yaml

  ManagedFile/sen.k8s.local-addons-dns-controller.addons.k8s.io-k8s-1.6
  	Location            	addons/dns-controller.addons.k8s.io/k8s-1.6.yaml

  ManagedFile/sen.k8s.local-addons-dns-controller.addons.k8s.io-pre-k8s-1.6
  	Location            	addons/dns-controller.addons.k8s.io/pre-k8s-1.6.yaml

  ManagedFile/sen.k8s.local-addons-kube-dns.addons.k8s.io-k8s-1.6
  	Location            	addons/kube-dns.addons.k8s.io/k8s-1.6.yaml

  ManagedFile/sen.k8s.local-addons-kube-dns.addons.k8s.io-pre-k8s-1.6
  	Location            	addons/kube-dns.addons.k8s.io/pre-k8s-1.6.yaml

  ManagedFile/sen.k8s.local-addons-limit-range.addons.k8s.io
  	Location            	addons/limit-range.addons.k8s.io/v1.5.0.yaml

  ManagedFile/sen.k8s.local-addons-rbac.addons.k8s.io-k8s-1.8
  	Location            	addons/rbac.addons.k8s.io/k8s-1.8.yaml

  ManagedFile/sen.k8s.local-addons-storage-aws.addons.k8s.io-v1.6.0
  	Location            	addons/storage-aws.addons.k8s.io/v1.6.0.yaml

  ManagedFile/sen.k8s.local-addons-storage-aws.addons.k8s.io-v1.7.0
  	Location            	addons/storage-aws.addons.k8s.io/v1.7.0.yaml

  Route/0.0.0.0/0
  	RouteTable          	name:sen.k8s.local
  	CIDR                	0.0.0.0/0
  	InternetGateway     	name:sen.k8s.local

  RouteTable/sen.k8s.local
  	VPC                 	name:sen.k8s.local
  	Shared              	false
  	Tags                	{KubernetesCluster: sen.k8s.local, kubernetes.io/cluster/sen.k8s.local: owned, kubernetes.io/kops/role: public, Name: sen.k8s.local}

  RouteTableAssociation/eu-west-1a.sen.k8s.local
  	RouteTable          	name:sen.k8s.local
  	Subnet              	name:eu-west-1a.sen.k8s.local

  SSHKey/kubernetes.sen.k8s.local-ac:e0:1e:36:3b:cb:2a:ff:4d:ed:87:47:30:20:8a:b9
  	KeyFingerprint      	0e:3d:f9:7b:96:66:ef:74:79:f1:28:d1:4a:ec:77:41

  Secret/admin

  Secret/kube

  Secret/kube-proxy

  Secret/kubelet

  Secret/system:controller_manager

  Secret/system:dns

  Secret/system:logging

  Secret/system:monitoring

  Secret/system:scheduler

  SecurityGroup/api-elb.sen.k8s.local
  	Description         	Security group for api ELB
  	VPC                 	name:sen.k8s.local
  	RemoveExtraRules    	[port=443]
  	Tags                	{kubernetes.io/cluster/sen.k8s.local: owned, Name: api-elb.sen.k8s.local, KubernetesCluster: sen.k8s.local}

  SecurityGroup/masters.sen.k8s.local
  	Description         	Security group for masters
  	VPC                 	name:sen.k8s.local
  	RemoveExtraRules    	[port=22, port=443, port=2380, port=2381, port=4001, port=4002, port=4789, port=179]
  	Tags                	{Name: masters.sen.k8s.local, KubernetesCluster: sen.k8s.local, kubernetes.io/cluster/sen.k8s.local: owned}

  SecurityGroup/nodes.sen.k8s.local
  	Description         	Security group for nodes
  	VPC                 	name:sen.k8s.local
  	RemoveExtraRules    	[port=22]
  	Tags                	{Name: nodes.sen.k8s.local, KubernetesCluster: sen.k8s.local, kubernetes.io/cluster/sen.k8s.local: owned}

  SecurityGroupRule/all-master-to-master
  	SecurityGroup       	name:masters.sen.k8s.local
  	SourceGroup         	name:masters.sen.k8s.local

  SecurityGroupRule/all-master-to-node
  	SecurityGroup       	name:nodes.sen.k8s.local
  	SourceGroup         	name:masters.sen.k8s.local

  SecurityGroupRule/all-node-to-node
  	SecurityGroup       	name:nodes.sen.k8s.local
  	SourceGroup         	name:nodes.sen.k8s.local

  SecurityGroupRule/api-elb-egress
  	SecurityGroup       	name:api-elb.sen.k8s.local
  	CIDR                	0.0.0.0/0
  	Egress              	true

  SecurityGroupRule/https-api-elb-0.0.0.0/0
  	SecurityGroup       	name:api-elb.sen.k8s.local
  	CIDR                	0.0.0.0/0
  	Protocol            	tcp
  	FromPort            	443
  	ToPort              	443

  SecurityGroupRule/https-elb-to-master
  	SecurityGroup       	name:masters.sen.k8s.local
  	Protocol            	tcp
  	FromPort            	443
  	ToPort              	443
  	SourceGroup         	name:api-elb.sen.k8s.local

  SecurityGroupRule/master-egress
  	SecurityGroup       	name:masters.sen.k8s.local
  	CIDR                	0.0.0.0/0
  	Egress              	true

  SecurityGroupRule/node-egress
  	SecurityGroup       	name:nodes.sen.k8s.local
  	CIDR                	0.0.0.0/0
  	Egress              	true

  SecurityGroupRule/node-to-master-tcp-1-2379
  	SecurityGroup       	name:masters.sen.k8s.local
  	Protocol            	tcp
  	FromPort            	1
  	ToPort              	2379
  	SourceGroup         	name:nodes.sen.k8s.local

  SecurityGroupRule/node-to-master-tcp-2382-4000
  	SecurityGroup       	name:masters.sen.k8s.local
  	Protocol            	tcp
  	FromPort            	2382
  	ToPort              	4000
  	SourceGroup         	name:nodes.sen.k8s.local

  SecurityGroupRule/node-to-master-tcp-4003-65535
  	SecurityGroup       	name:masters.sen.k8s.local
  	Protocol            	tcp
  	FromPort            	4003
  	ToPort              	65535
  	SourceGroup         	name:nodes.sen.k8s.local

  SecurityGroupRule/node-to-master-udp-1-65535
  	SecurityGroup       	name:masters.sen.k8s.local
  	Protocol            	udp
  	FromPort            	1
  	ToPort              	65535
  	SourceGroup         	name:nodes.sen.k8s.local

  SecurityGroupRule/ssh-external-to-master-0.0.0.0/0
  	SecurityGroup       	name:masters.sen.k8s.local
  	CIDR                	0.0.0.0/0
  	Protocol            	tcp
  	FromPort            	22
  	ToPort              	22

  SecurityGroupRule/ssh-external-to-node-0.0.0.0/0
  	SecurityGroup       	name:nodes.sen.k8s.local
  	CIDR                	0.0.0.0/0
  	Protocol            	tcp
  	FromPort            	22
  	ToPort              	22

  Subnet/eu-west-1a.sen.k8s.local
  	ShortName           	eu-west-1a
  	VPC                 	name:sen.k8s.local
  	AvailabilityZone    	eu-west-1a
  	CIDR                	172.20.32.0/19
  	Shared              	false
  	Tags                	{KubernetesCluster: sen.k8s.local, kubernetes.io/cluster/sen.k8s.local: owned, kubernetes.io/role/elb: 1, SubnetType: Public, Name: eu-west-1a.sen.k8s.local}

  VPC/sen.k8s.local
  	CIDR                	172.20.0.0/16
  	EnableDNSHostnames  	true
  	EnableDNSSupport    	true
  	Shared              	false
  	Tags                	{Name: sen.k8s.local, KubernetesCluster: sen.k8s.local, kubernetes.io/cluster/sen.k8s.local: owned}

  VPCDHCPOptionsAssociation/sen.k8s.local
  	VPC                 	name:sen.k8s.local
  	DHCPOptions         	name:sen.k8s.local

Must specify --yes to apply changes

Cluster configuration has been created.

Suggestions:
 * list clusters with: kops get cluster
 * edit this cluster with: kops edit cluster sen.k8s.local
 * edit your node instance group: kops edit ig --name=sen.k8s.local nodes
 * edit your master instance group: kops edit ig --name=sen.k8s.local master-eu-west-1a

Finally configure your cluster with: kops update cluster sen.k8s.local --yes



5. Create the cluster with  kops update cluster --name ${KOPS_CLUSTER_NAME} --yes

root@Helsing:/usr/local/bin# kops update cluster --name sen.k8s.local --yes
I1027 11:48:41.820844   28507 apply_cluster.go:505] Gossip DNS: skipping DNS validation
I1027 11:48:42.045859   28507 executor.go:103] Tasks: 0 done / 77 total; 30 can run
I1027 11:48:42.608488   28507 vfs_castore.go:735] Issuing new certificate: "apiserver-aggregator-ca"
I1027 11:48:42.663036   28507 vfs_castore.go:735] Issuing new certificate: "ca"
I1027 11:48:42.883397   28507 executor.go:103] Tasks: 30 done / 77 total; 24 can run
I1027 11:48:43.368225   28507 vfs_castore.go:735] Issuing new certificate: "kubecfg"
I1027 11:48:43.462664   28507 vfs_castore.go:735] Issuing new certificate: "kube-scheduler"
I1027 11:48:43.467770   28507 vfs_castore.go:735] Issuing new certificate: "kubelet"
I1027 11:48:43.495626   28507 vfs_castore.go:735] Issuing new certificate: "apiserver-aggregator"
I1027 11:48:43.580685   28507 vfs_castore.go:735] Issuing new certificate: "kube-controller-manager"
I1027 11:48:43.581047   28507 vfs_castore.go:735] Issuing new certificate: "apiserver-proxy-client"
I1027 11:48:43.605366   28507 vfs_castore.go:735] Issuing new certificate: "kube-proxy"
I1027 11:48:43.641858   28507 vfs_castore.go:735] Issuing new certificate: "kops"
I1027 11:48:43.662370   28507 vfs_castore.go:735] Issuing new certificate: "kubelet-api"
I1027 11:48:43.953402   28507 executor.go:103] Tasks: 54 done / 77 total; 19 can run
I1027 11:48:44.264269   28507 launchconfiguration.go:380] waiting for IAM instance profile "nodes.sen.k8s.local" to be ready
I1027 11:48:44.285667   28507 launchconfiguration.go:380] waiting for IAM instance profile "masters.sen.k8s.local" to be ready
I1027 11:48:54.716604   28507 executor.go:103] Tasks: 73 done / 77 total; 3 can run
I1027 11:48:55.431291   28507 vfs_castore.go:735] Issuing new certificate: "master"
I1027 11:48:55.642315   28507 executor.go:103] Tasks: 76 done / 77 total; 1 can run
I1027 11:48:55.898375   28507 executor.go:103] Tasks: 77 done / 77 total; 0 can run
I1027 11:48:55.988206   28507 update_cluster.go:290] Exporting kubecfg for cluster
kops has set your kubectl context to sen.k8s.local

Cluster is starting.  It should be ready in a few minutes.

Suggestions:
 * validate cluster: kops validate cluster
 * list nodes: kubectl get nodes --show-labels
 * ssh to the master: ssh -i ~/.ssh/id_rsa admin@api.sen.k8s.local
 * the admin user is specific to Debian. If not using Debian please use the appropriate user based on your OS.
 * read about installing addons at: https://github.com/kubernetes/kops/blob/master/docs/addons.md.




6. Validate the cluster, this may take a while to come online, with kops validate cluster.

root@Helsing:/usr/local/bin# kops validate cluster
Validating cluster sen.k8s.local

INSTANCE GROUPS
NAME			ROLE	MACHINETYPE	MIN	MAX	SUBNETS
master-eu-west-1a	Master	m3.medium	1	1	eu-west-1a
nodes			Node	t2.medium	5	5	eu-west-1a

NODE STATUS
NAME						ROLE	READY
ip-172-20-44-194.eu-west-1.compute.internal	node	True
ip-172-20-46-148.eu-west-1.compute.internal	node	True
ip-172-20-48-6.eu-west-1.compute.internal	node	True
ip-172-20-51-147.eu-west-1.compute.internal	master	True
ip-172-20-53-206.eu-west-1.compute.internal	node	True
ip-172-20-55-205.eu-west-1.compute.internal	node	True



7. Verify that the instances of the cluster are up and available.

root@Helsing:/home/cwills# aws ec2 get-console-output --instance-id i-02dfac82297ceedfa
i-02dfac82297ceedfa	[    0.000000] Initializing cgroup subsys cpuset
[    0.000000] Initializing cgroup subsys cpu
[    0.000000] Initializing cgroup subsys cpuacct
[    0.000000] Linux version 4.4.148-k8s (root@a18f4df3fb27) (gcc version 8.2.0 (Debian 8.2.0-4) ) #1 SMP Thu Aug 16 19:29:46 UTC 2018
[    0.000000] Command line: BOOT_IMAGE=/boot/vmlinuz-4.4.148-k8s root=UUID=1b27bf9a-e814-4924-9968-d377fb773ab7 ro init=/bin/systemd cgroup_enable=memory oops=panic panic=10 console=ttyS0
[    0.000000] x86/fpu: xstate_offset[2]:  576, xstate_sizes[2]:  256
[    0.000000] x86/fpu: Supporting XSAVE feature 0x01: 'x87 floating point registers'
[    0.000000] x86/fpu: Supporting XSAVE feature 0x02: 'SSE registers'
[    0.000000] x86/fpu: Supporting XSAVE feature 0x04: 'AVX registers'
[    0.000000] x86/fpu: Enabled xstate features 0x7, context size is 832 bytes, using 'standard' format.
[    0.000000] e820: BIOS-provided physical RAM map:
[    0.000000] BIOS-e820: [mem 0x0000000000000000-0x000000000009dfff] usable
[    0.000000] BIOS-e820: [mem 0x000000000009e000-0x000000000009ffff] reserved
[    0.000000] BIOS-e820: [mem 0x00000000000e0000-0x00000000000fffff] reserved
[    0.000000] BIOS-e820: [mem 0x0000000000100000-0x00000000efffffff] usable
[    0.000000] BIOS-e820: [mem 0x00000000fc000000-0x00000000ffffffff] reserved
[    0.000000] NX (Execute Disable) protection: active
[    0.000000] SMBIOS 2.7 present.
[    0.000000] Hypervisor detected: Xen
[    0.000000] Xen version 4.2.
[    0.000000] Netfront and the Xen platform PCI driver have been compiled for this kernel: unplug emulated NICs.
[    0.000000] Blkfront and the Xen platform PCI driver have been compiled for this kernel: unplug emulated disks.
[    0.000000] You might have to change the root device

.
.
.- Truncated for brevity -
.

Debian GNU/Linux 8 ip-172-20-51-147 ttyS0

ip-172-20-51-147 login: [   28.857638] EXT4-fs (xvda1): resizing filesystem from 2096640 to 16775364 blocks
[   29.756532] bridge: automatic filtering via arp/ip/ip6tables has been deprecated. Update your scripts to load br_netfilter if you need this.
[   29.770620] Bridge firewalling registered
[   29.806900] nf_conntrack version 0.5.0 (16384 buckets, 65536 max)
[   29.949255] ip_tables: (C) 2000-2006 Netfilter Core Team
[   30.087794] EXT4-fs (xvda1): resized filesystem to 16775364
[   30.362970] Initializing XFRM netlink socket
[   30.451170] IPv6: ADDRCONF(NETDEV_UP): docker0: link is not ready
[   31.542240] EXT4-fs (xvdc): mounting ext3 file system using the ext4 subsystem
[   31.553128] EXT4-fs (xvdc): mounted filesystem with ordered data mode. Opts: (null)
ci-info: +++++++++++++++++++++++++++++++++++++++Authorized keys from /home/admin/.ssh/authorized_keys for user admin+++++++++++++++++++++++++++++++++++++++
ci-info: +---------+-------------------------------------------------+---------+--------------------------------------------------------------------------+
ci-info: | Keytype |                Fingerprint (md5)                | Options |                                 Comment                                  |
ci-info: +---------+-------------------------------------------------+---------+--------------------------------------------------------------------------+
ci-info: | ssh-rsa | ac:e0:1e:36:3b:cb:2a:ff:4d:ed:87:47:30:20:8a:b9 |    -    | kubernetes.sen.k8s.local-ac:e0:1e:36:3b:cb:2a:ff:4d:ed:87:47:30:20:8a:b9 |
ci-info: +---------+-------------------------------------------------+---------+--------------------------------------------------------------------------+
ec2: 
ec2: #############################################################
ec2: -----BEGIN SSH HOST KEY FINGERPRINTS-----
ec2: 1024 2e:5c:0a:b2:41:fc:04:93:1f:05:37:39:f9:d0:37:82 /etc/ssh/ssh_host_dsa_key.pub (DSA)
ec2: 256 7d:19:81:f4:f7:b6:6e:d2:8c:ee:0a:30:e0:2d:87:cf /etc/ssh/ssh_host_ecdsa_key.pub (ECDSA)
ec2: 2048 4d:a2:a3:9d:78:7f:d3:b2:06:04:9a:33:3e:a6:0d:68 /etc/ssh/ssh_host_rsa_key.pub (RSA)
ec2: -----END SSH HOST KEY FINGERPRINTS-----
ec2: #############################################################
-----BEGIN SSH HOST KEY KEYS-----
ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBJGqca5agbYRx1kl01Y7VkSoeyewcLzmdmGxr0OD+EkpMxoCbrISpsHWBpBP8rsZaSM1ZOcxgJCKeJN3n9ln8ug= root@ip-172-20-51-147
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDYb0V2SqmrET0ElOMYTkx3a2ckO4BvYOT9w7KfJPx7W/Qf2DUR7qhyofJUNm8y6CStF5Tg2u8GJutEYRNQyZphbqb4t6WG7P/5xq5PP/zRn/OKl4ofARKfyTRbyrog8s7uHWJqkTWsFYY6br3QWNtRw3bjiWUZAacDwCD5fcvEHODw7T31lIkK3Vcc0gsmieZk9Ovb6qF7H06YsPBCJWjJIt1H4C43YkSzqAN44arAJp4l95ih1gI0AY6f95gOS8Fj/cRljbj7GX7F9IO36BOFTKlzyt6VGwQBEygvMuxr8lOHmJnHf4uyNUN6/X2NLaFpwgr/ye3sz+gUhwVUmR5j root@ip-172-20-51-147
-----END SSH HOST KEY KEYS-----
[   95.820321] blkfront: xvdu: barrier or flush: disabled; persistent grants: disabled; indirect descriptors: enabled;
[   97.251016] EXT4-fs (xvdu): mounted filesystem with ordered data mode. Opts: (null)
[   97.402466] blkfront: xvdv: barrier or flush: disabled; persistent grants: disabled; indirect descriptors: enabled;
[   99.439239] EXT4-fs (xvdv): mounted filesystem with ordered data mode. Opts: (null)
[  148.437847] IPVS: Registered protocols (TCP, UDP, SCTP, AH, ESP)
[  148.448094] IPVS: Connection hash table configured (size=4096, memory=64Kbytes)
[  148.481554] IPVS: Creating netns size=2192 id=0
[  148.488357] IPVS: ipvs loaded.
[  157.169296] Netfilter messages via NETLINK v0.30.
[  157.192392] ip_set: protocol 6
	2018-10-27T10:52:13.000Z
root@Helsing:/home/cwills# 


8. Verify that the cluster nodes have joined the cluster

root@Helsing:/home/cwills/Sensyne/exercise-deployment/k8s-mysql-wordpress-deployment# kubectl get nodes
NAME                                          STATUS   ROLES    AGE   VERSION
ip-172-20-44-194.eu-west-1.compute.internal   Ready    node     10h   v1.10.6
ip-172-20-46-148.eu-west-1.compute.internal   Ready    node     10h   v1.10.6
ip-172-20-48-6.eu-west-1.compute.internal     Ready    node     10h   v1.10.6
ip-172-20-51-147.eu-west-1.compute.internal   Ready    master   10h   v1.10.6
ip-172-20-53-206.eu-west-1.compute.internal   Ready    node     10h   v1.10.6
ip-172-20-55-205.eu-west-1.compute.internal   Ready    node     10h   v1.10.6



9. Install/configure the GUI

root@Helsing:/usr/local/bin# kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/src/deploy/recommended/kubernetes-dashboard.yaml
secret/kubernetes-dashboard-certs created
serviceaccount/kubernetes-dashboard created
role.rbac.authorization.k8s.io/kubernetes-dashboard-minimal created
rolebinding.rbac.authorization.k8s.io/kubernetes-dashboard-minimal created
deployment.apps/kubernetes-dashboard created
service/kubernetes-dashboard created


10. Edit the GUI config to expose the port to external NIC of the master node. Change the default type:ClusterIP setting to NodePort 

root@Helsing:/usr/local/bin# kubectl edit service kubernetes-dashboard -n kube-system
 
# Please edit the object below. Lines beginning with a '#' will be ignored,
# and an empty file will abort the edit. If an error occurs while saving this file will be
# reopened with the relevant failures.
#
apiVersion: v1
kind: Service
metadata:
  annotations:
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"v1","kind":"Service","metadata":{"annotations":{},"labels":{"k8s-app":"kubernetes-dashboard"},"name":"kubernetes-dashboard","namespace":"kube-system"},"spec":{"ports":[{"port":443,"targetPort":8443}],"selector":{"k8s-app":"kubernetes-dashboard"}}}
  creationTimestamp: 2018-10-27T13:13:06Z
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
  namespace: kube-system
  resourceVersion: "14102"
  selfLink: /api/v1/namespaces/kube-system/services/kubernetes-dashboard
  uid: 0acd1b85-d9ea-11e8-a906-0a98185cfadc
spec:
  clusterIP: 100.69.114.252
  externalTrafficPolicy: Cluster
  ports:
  - nodePort: 30183
    port: 443
    protocol: TCP
    targetPort: 8443
  selector:
    k8s-app: kubernetes-dashboard
  sessionAffinity: None
  type: NodePort
status:
  loadBalancer: {}




NOTES:

If you need to delete the dashboard for any reason you should be able to run the following:



kubectl get secret,sa,role,rolebinding,services,deployments --namespace=kube-system | grep dashboard

kubectl delete deployment kubernetes-dashboard --namespace=kube-system 
kubectl delete service kubernetes-dashboard  --namespace=kube-system 
kubectl delete role kubernetes-dashboard-minimal --namespace=kube-system 
kubectl delete rolebinding kubernetes-dashboard-minimal --namespace=kube-system
kubectl delete sa kubernetes-dashboard --namespace=kube-system 
kubectl delete secret kubernetes-dashboard-certs --namespace=kube-system
kubectl delete secret kubernetes-dashboard-key-holder --namespace=kube-system


11. Set the permissions for the dashboard, if these are not set, the GUI will error and will be unusable.

root@Helsing:/usr/local/bin# kubectl create clusterrolebinding kubernetes-dashboard -n kube-system --clusterrole=cluster-admin --serviceaccount=kube-system:kubernetes-dashboard
clusterrolebinding.rbac.authorization.k8s.io/kubernetes-dashboard created

12. Find the port and the token you need to connect to the GUI via https/http.

kubectl get services kubernetes-dashboard -n kube-system 

root@Helsing:/home/cwills/Sensyne/exercise-deployment/k8s-mysql-wordpress-deployment# kubectl get services kubernetes-dashboard -n kube-system 
NAME                   TYPE       CLUSTER-IP       EXTERNAL-IP   PORT(S)         AGE
kubernetes-dashboard   NodePort   100.69.114.252   <none>        443:30183/TCP   8h



13. Find the token string 

root@Helsing:/home/cwills/Sensyne/exercise-deployment/k8s-mysql-wordpress-deployment# kubectl describe serviceaccount kubernetes-dashboard -n kube-system
Name:                kubernetes-dashboard
Namespace:           kube-system
Labels:              k8s-app=kubernetes-dashboard
Annotations:         kubectl.kubernetes.io/last-applied-configuration:
                       {"apiVersion":"v1","kind":"ServiceAccount","metadata":{"annotations":{},"labels":{"k8s-app":"kubernetes-dashboard"},"name":"kubernetes-das...
Image pull secrets:  <none>
Mountable secrets:   kubernetes-dashboard-token-v42cv
Tokens:              kubernetes-dashboard-token-v42cv
Events:              <none>


root@Helsing:/home/cwills/Sensyne/exercise-deployment/k8s-mysql-wordpress-deployment# kubectl describe secrets kubernetes-dashboard-token-v42cv -n kube-system  


ame:         kubernetes-dashboard-token-v42cv
Namespace:    kube-system
Labels:       <none>
Annotations:  kubernetes.io/service-account.name: kubernetes-dashboard
              kubernetes.io/service-account.uid: 0a992543-d9ea-11e8-a906-0a98185cfadc

Type:  kubernetes.io/service-account-token

Data
====
token:      eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3V
udC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJrdWJlcm5ldGVzLWRhc2hib2FyZC10b2t
lbi12NDJjdiIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50Lm5hbWUiOiJrdWJlcm5ldGVzLWRhc2hib2FyZCIsImt1YmVybmV0ZXM
uaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50LnVpZCI6IjBhOTkyNTQzLWQ5ZWEtMTFlOC1hOTA2LTBhOTgxODVjZmFkYyIsInN1YiI6InN5c3RlbTpzZXJ
2aWNlYWNjb3VudDprdWJlLXN5c3RlbTprdWJlcm5ldGVzLWRhc2hib2FyZCJ9.bdcVBFnuNjvv3313R8IZ49DHSjZEzU6sFJINhh2z6fmBfOsXnXW1EAKxQKvCumiNif
RTvT9Yr2K1YQJcOgZnvoH7cN1XLn1OS_DPIj3D-fIoaBxUfWyCA5COmDvE1D-BRzx05RPp8JXTe8SNYg38Iyqs1rNfF9hONFrdABPYoelaL6pYgGOtglxrT6G5snyxJi
nv05mkCyoXJovFG0U1fhhcZn0kqYcngW4R98LSyeaP7FFUGyPshfVPgjJ1zp8HUaxs0TISEA4HbTj_rYD0_r3HTEwNyl6oZr0MTkVDdcrVgEMqNfaeQp_2nFoYjGFECk
ehqpdcEJmXeHgjfO0D2Q
ca.crt:     1042 bytes
namespace:  11 bytes




Adjustments to the VPC security groups  *will* be required to allow HTTP/HTTPS such that you're able to point a browser to the newly configure gui. 

https://<public address of master instance>:<port>

Example: https://18.202.235.65:30183








14. Deploy wordpress, mysql, application and database layers.




15. Create a password for  mysql

root@Helsing:/usr/local/bin# kubectl create secret generic mysql-pass --from-literal=password=admin

Check the created secrets:
kubectl get secrets

root@Helsing:/usr/local/bin# kubectl get secrets
NAME                  TYPE                                  DATA   AGE
default-token-zrkbw   kubernetes.io/service-account-token   3      5h
mysql-pass            Opaque                                1      24s
root@Helsing:/usr/local/bin# 


16. Create a persistent volume(s) for both mysql and wordpress.


Create a createpv.yaml as below.

kind: PersistentVolume
apiVersion: v1
metadata:  
  name: persistent-storage  
  labels:    
    type: local
spec:  
  storageClassName: manual  
  capacity:    
    storage: 10Gi  
  persistentVolumeReclaimPolicy: Recycle  
  accessModes:
    - ReadWriteOnce  
  hostPath:    
    path: "/var/lib/my-sql"
---
kind: PersistentVolume
apiVersion: v1
metadata:  
  name: persistent-storage-wp
  labels:    
    type: local
spec:  
  storageClassName: manual  
  capacity:    
    storage: 10Gi  
  persistentVolumeReclaimPolicy: Recycle  
  accessModes:
    - ReadWriteOnce  
  hostPath:    
    path: "/var/lib/wordpress"



Create the physical volume

root@Helsing:/home/cwills/Sensyne/wordpress-deployment# kubectl create -f createpv.yaml
persistentvolume/persistent-storage created
persistentvolume/persistent-storage-wp created

Check the pv has been created, this can be done in the GUI.



17. Create the  mysql manifest. create-mysql-deployment.yaml

 

apiVersion: v1
kind: Service
metadata:
  name: wordpress-mysql
  labels:
    app: wordpress
spec:
  ports:
    - port: 3306
  selector:
    app: wordpress
    tier: mysql
  clusterIP: None
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: mysql-pv-claim
  labels:
    app: wordpress
spec:
  storageClassName: manual
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 2Gi
---
apiVersion: apps/v1 # for versions before 1.9.0 use apps/v1beta2
kind: Deployment
metadata:
  name: wordpress-mysql
  labels:
    app: wordpress
spec:
  selector:
    matchLabels:
      app: wordpress
      tier: mysql
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: wordpress
        tier: mysql
    spec:
      containers:
      - image: mysql:5.6
        name: mysql
        env:
        - name: MYSQL_ROOT_PASSWORD
          valueFrom:
            secretKeyRef:
              name: mysql-pass
              key: password
        ports:
        - containerPort: 3306
          name: mysql
        volumeMounts:
        - name: mysql-persistent-storage
          mountPath: /var/lib/mysql
      volumes:
      - name: mysql-persistent-storage
        persistentVolumeClaim:
          claimName: mysql-pv-claim


18. Create the wordpress manifest create-wordpress-deployment.yaml

apiVersion: v1
kind: Service
metadata:
  name: wordpress
  labels:
    app: wordpress
spec:
  ports:
    - port: 80
  selector:
    app: wordpress
    tier: frontend
  type: LoadBalancer
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: wp-pv-claim
  labels:
    app: wordpress
spec:
  storageClassName: manual
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 2Gi
---
apiVersion: apps/v1 # for versions before 1.9.0 use apps/v1beta2
kind: Deployment
metadata:
  name: wordpress
  labels:
    app: wordpress
spec:
  selector:
    matchLabels:
      app: wordpress
      tier: frontend
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: wordpress
        tier: frontend
    spec:
      containers:
      - image: wordpress:4.8-apache
        name: wordpress
        env:
        - name: WORDPRESS_DB_HOST
          value: wordpress-mysql
        - name: WORDPRESS_DB_PASSWORD
          valueFrom:
            secretKeyRef:
              name: mysql-pass
              key: password
        ports:
        - containerPort: 80
          name: wordpress
        volumeMounts:
        - name: wordpress-persistent-storage
          mountPath: /var/www/html
      volumes:
      - name: wordpress-persistent-storage
        persistentVolumeClaim:
          claimName: wp-pv-claim



19. Create a node port or better still a loadbalancer to allow connection to the running service.

root@Helsing:/home/cwills/Sensyne/wordpress-deployment# kubectl get service,pvc,deployment,pods
NAME                      TYPE           CLUSTER-IP       EXTERNAL-IP                                                               PORT(S)        AGE
service/kubernetes        ClusterIP      100.64.0.1       <none>                                                                    443/TCP        6h
service/wordpress         LoadBalancer   100.65.131.233   a41d86c4eda0e11e8a9060a98185cfad-1316618449.eu-west-1.elb.amazonaws.com   80:30061/TCP   13m
service/wordpress-mysql   ClusterIP      None             <none>                                                                    3306/TCP       1h

NAME                                   STATUS   VOLUME                  CAPACITY   ACCESS MODES   STORAGECLASS   AGE
persistentvolumeclaim/mysql-pv-claim   Bound    persistent-storage-wp   10Gi       RWO            manual         1h
persistentvolumeclaim/wp-pv-claim      Bound    persistent-storage      10Gi       RWO            manual         13m

NAME                                    DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
deployment.extensions/wordpress         1         1         1            1           5m
deployment.extensions/wordpress-mysql   1         1         1            1           1h

NAME                                  READY   STATUS    RESTARTS   AGE
pod/wordpress-7bdfd5557c-nhd89        1/1     Running   0          5m
pod/wordpress-mysql-bcc89f687-q5hqx   1/1     Running   0          1h




root@Helsing:/home/cwills/Sensyne/wordpress-deployment# kubectl edit svc wordpress
service/wordpress edited
root@Helsing:/home/cwills/Sensyne/wordpress-deployment# kubectl get service,pvc,deployment,pods
NAME                      TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)        AGE
service/kubernetes        ClusterIP   100.64.0.1       <none>        443/TCP        6h
service/wordpress         NodePort    100.65.131.233   <none>        80:30061/TCP   14m
service/wordpress-mysql   ClusterIP   None             <none>        3306/TCP       1h


To test the configuration the service could be redefined as type: NodePort allowing you to point a browesr at the address of the master and port 30061 (in this case).



